#!/usr/bin/python
import sys, re, Queue
from collections import defaultdict

t = int(sys.stdin.readline())

class Node:
    def __init__( self, n ):
        self.n = n
        self.ladderx = 0
        self.laddery = 0
        self.snakex = 0
        self.snakey = 0
    def n(self):
        return self.n
    def setLadder( self, x, y ):
        self.ladderx = x
        self.laddery = y
    def ladderx( self ):
        return self.ladderx
    def laddery( self ):
        return self.laddery
    def setSnake( self, x, y ):
        self.snakex = x
        self.snakey = y
    def snakex( self ):
        return self.snakex
    def snakey( self ):
        return self.snakey

class Path:
    def __init__( self, n, ladders ):
        self.n = n
        self.rolls = 0
        self.lad = ladders
    def __cmp__(self, other):
        heuristicSelf = float(self.rolls) + float(ladderHeuristic( self.lad, self.getLocation() ) )
        heuristicOther = float(other.rolls) + float(ladderHeuristic( other.lad, other.getLocation()))
        return cmp( heuristicSelf, heuristicOther)
    def setRolls( self, r ):
        self.rolls = r
    def rolls( self ):
        return self.rolls
    def getLocation( self ):
        return self.n

def ladderHeuristic( lad,  n ):
    h = 0
    i = int(n)
    prev = n
    keys = lad.keys()
    for k in keys:
        if k >= i:
            h += 1
            i = lad[k]
    return h

# for each test case
for i in range( 0, t ):
    # initial set up
    board = list()
    for i in range(1, 101):
        board.append( Node(i) )
    l = sys.stdin.readline()
    ladders = int( re.sub(r',.*', '', l ))
    snakes = int ( re.sub(r'[0-9]+,', '', l))
    l = sys.stdin.readline() 
    l = re.sub(r'\s*$', '', l)
    l = re.split(' ', l)
    ladderDict = defaultdict(int)
    for pair in l:
        start = re.sub(r',.*', '', pair)
        end = re.sub(r'[0-9]+,', '', pair)
        ladderDict[start] = end
        board[int(start)-1].setLadder(int(start),(end))
    l = sys.stdin.readline()
    l = re.sub(r'\s*$', '', l)
    l = re.split(' ', l)
    for pair in l:
        start = re.sub(r',.*', '', pair)
        end = re.sub(r'[0-9]+,', '', pair)
        board[int(start)-1].setSnake(int(start),int(end))
   
    # begin game
    position = 1
    rolls = 1
    queue = Queue.PriorityQueue()
    p = Path(1, ladderDict)
    queue.put( p )
    while not queue.empty():
        p = queue.get()
        # stop when popped node of 100
        if p.getLocation() == 100: break
        r = p.rolls
        # add nodes for each outcome
        best = -1
        i = 6
        while i > 0:
            newLocation = i+ int(p.getLocation())
            if newLocation <= 100:
                nde = board[ newLocation - 1]
                if nde.laddery != 0:
                    newLocation = nde.laddery
                # Don't go backward
                if nde.snakey == 0:
                    # Don't add single hop and not the best move
                    if newLocation > best:
                        best = newLocation
                        newPath = Path( newLocation, ladderDict )
                        newPath.setRolls( r + 1 )
                        queue.put( newPath )
            i -= 1
                
    print p.rolls

